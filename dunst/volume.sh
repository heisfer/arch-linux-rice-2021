#!/bin/bash

# You can call this script like this:
# $./volume.sh up
# $./volume.sh down
# $./volume.sh mute

function get_volume {
    pactl list | grep '%' | head -n 3 | cut -d '/' -f 2  | cut -d '%' -f 1 | tail -1
}

function is_mute {
    pactl get-sink-mute @DEFAULT_SINK@ | cut -d ":" -f 2 | sed -e 's/^[[:space:]]*//'
}

function send_notification {
    DIR=`dirname "$0"`
    volume=`get_volume`
    # Make the bar with the special character ─ (it's not dash -)
    # https://en.wikipedia.org/wiki/Box-drawing_character
#bar=$(seq -s "─" $(($volume/5)) | sed 's/[0-9]//g')
if [ "$volume" = "0" ]; then
        icon_name="/usr/share/icons/Faba/48x48/notifications/notification-audio-volume-muted.svg"
        $DIR/notify-send.sh "$volume""      " -i "$icon_name" -t 2000 -h int:value:"$volume" -h string:synchronous:"─" --replace=555
    else
	if [  "$volume" -lt "10" ]; then
	     icon_name="/usr/share/icons/Faba/48x48/notifications/notification-audio-volume-low.svg"
         $DIR/notify-send.sh "$volume""     " -i "$icon_name" --replace=555 -t 2000
    else
        if [ "$volume" -lt "30" ]; then
            icon_name="/usr/share/icons/Faba/48x48/notifications/notification-audio-volume-low.svg"
        else
            if [ "$volume" -lt "70" ]; then
                icon_name="/usr/share/icons/Faba/48x48/notifications/notification-audio-volume-medium.svg"
            else
                icon_name="/usr/share/icons/Faba/48x48/notifications/notification-audio-volume-high.svg"
            fi
        fi
    fi
fi
msgTag="Volume"
dunstify -a "changeVolume" -i "$icon_name" -u normal -h string:x-dunst-stack-tag:$msgTag -h int:value:"$volume" "Volume: ${volume}%"

}

case $1 in
    up)
	pactl set-sink-volume @DEFAULT_SINK@ +5%
	send_notification
	;;
    down)
	pactl set-sink-volume @DEFAULT_SINK@ -5%
	send_notification
	;;
    mute)
    pactl set-sink-mute @DEFAULT_SINK@ toggle
    mute=$(is_mute)
	if [[ "$mute" == yes ]]; then

        DIR=`dirname "$0"`
        msgTag="Volume"
        dunstify -a "changeVolume" -i "/usr/share/icons/Faba/48x48/notifications/notification-audio-volume-muted.svg"  -u normal -h string:x-dunst-stack-tag:$msgTag "Mute"
        $DIR/notify-send.sh --replace=555 -u normal "" -t 2000
	else
	    send_notification
	fi
	;;
esac
