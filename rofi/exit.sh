#!/bin/bash
logout=' Log out'
power_off=' Power off'
restart=' Restart'
lock=' Lock'

chosen=$(printf '%s;%s;%s;%s\n' "$logout" "$power_off" "$restart" "$lock" \
    | rofi -dmenu -sep ';' -select-row 2 -p "Power option")

case "$chosen" in
    "$lock")
	betterlockscreen -l --time-format '%I:%M %p'
    ;;
    "$power_off")
        poweroff
    ;;
    "$restart")
        reboot
    ;;
    "$logout")
        i3-msg exit
    ;;
esac
	
