#!/bin/bash

search=" Search"
network=" Wifi"
bluetooth=" Bluetooth"
ssh=" SSH"
power=" Power Settings"

chosen=$(printf '%s;%s;%s;%s;%s\n' "$search" "$network" "$bluetooth" "$ssh" "$power" \
    | rofi -dmenu -sep ';' -select-row 2 -p "Menu")

case "$chosen" in
  "$search")
    rofi -show drun -show-icons -modi drun,run -p "dsad";;
  "$network")
    exec ~/.config/rofi/menu/iwd.sh;;
  "$bluetooth")
    exec ~/.config/rofi/menu/bt.sh;;
  "$ssh")
    rofi -show ssh -modi ssh;;
  "$power")
    exec ~/.config/rofi/exit.sh;;
esac
